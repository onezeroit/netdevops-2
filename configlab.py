import telnetlib
import subprocess
import sys

user = "cisco"
password = "cisco"
#IOS edge switch
tn = telnetlib.Telnet("10.10.20.172")
tn.read_until(b"Password: ")
tn.write(password.encode('ascii') + b"\n")
tn.write(b"enable\n")
tn.read_until(b"Password: ")
tn.write(password.encode('ascii') + b"\n")
tn.write(b"conf t\n")
tn.write(b"username cisco privilege 15 password cisco\n")
tn.write(b"ip domain-name oefen.lab\n")
tn.write(b"crypto key generate rsa mod 2048\n")
tn.write(b"line vty 0 4\n")
tn.write(b"login local\n")
tn.write(b"transport input ssh telnet\n")
tn.write(b"exit\n")
tn.write(b"ip scp server enable\n")
tn.write(b"archive\n")
tn.write(b"path flash:archive\n")
tn.write(b"end\n")
tn.write(b"exit\n")
print(tn.read_all().decode('ascii'))
#IOS overig
iosdevices = {'10.10.20.175','10.10.20.176','10.10.20.181'}
for device in iosdevices:
    tn = telnetlib.Telnet(device)
    tn.read_until(b"Username: ")
    tn.write(user.encode('ascii') + b"\n")
    tn.read_until(b"Password: ")
    tn.write(password.encode('ascii') + b"\n")
    tn.write(b"conf t\n")
    tn.write(b"ip scp server enable\n")
    tn.write(b"archive\n")
    tn.write(b"path flash:archive\n")
    tn.write(b"end\n")
    tn.write(b"exit\n")
    print(tn.read_all().decode('ascii'))
#IOSXR
iosxrdevices = {'10.10.20.173','10.10.20.174'}
for device in iosxrdevices:
    tn = telnetlib.Telnet(device)
    tn.read_until(b"Username: ")
    tn.write(user.encode('ascii') + b"\n")
    tn.read_until(b"Password: ")
    tn.write(password.encode('ascii') + b"\n")
    tn.write(b"crypto key generate rsa\n")
    tn.read_until(b"[2048]: ")
    tn.write(b"\n")
    tn.write(b"conf\n")
    tn.write(b"ssh server v2\n")
    tn.write(b"ssh server vrf Mgmt-intf\n")
    tn.write(b"line default transport input telnet ssh\n")
    tn.write(b"xml agent tty iteration off\n")
    tn.write(b"commit\n")
    tn.read_until(b"(config)#")
    tn.write(b"end\n")
    tn.write(b"exit\n")
    print(tn.read_all().decode('ascii'))
#IOS overig
nxosdevices = {'10.10.20.177','10.10.20.178'}
for device in nxosdevices:
    tn = telnetlib.Telnet(device)
    tn.read_until(b"login: ")
    tn.write(user.encode('ascii') + b"\n")
    tn.read_until(b"Password: ")
    tn.write(password.encode('ascii') + b"\n")
    tn.write(b"config t\n")
    tn.write(b"feature scp-server\n")
    tn.write(b"feature nxapi\n")
    tn.write(b"end\n")
    tn.write(b"exit\n")
    print(tn.read_all().decode('ascii'))
#ASA
asa='10.10.20.171'
tn = telnetlib.Telnet(asa)
tn.read_until(b"Password:", timeout=4)
tn.write(password.encode('ascii') + b"\n")
tn.read_until(b">", timeout=4)
tn.write(b"enable\n")
tn.read_until(b"Password:", timeout=4)
tn.write(password.encode('ascii') + b"\n")
tn.write(b"config t\n")
tn.write(b"rest-api image boot:/asa-restapi-132346-lfbff-k8.SPA\n")
tn.write(b"rest-api agent\n")
tn.write(b"end\n")
tn.write(b"exit\n")
print(tn.read_all().decode('ascii'))

# install python packages
packages = [ "requests", "napalm", "napalm-asa" ]
for package in packages:
    subprocess.check_call([sys.executable, '-m', 'pip', 'install', package])

# fix napalm-asa dependancy error
napalm_asa_file = "/home/developer/py3venv/lib/python3.6/site-packages/napalm_asa/asa.py"
with open(napalm_asa_file, "r") as f:
    lines = f.readlines()
with open(napalm_asa_file, "w") as f:
    for line in lines:
        if line.strip("\n") != "from napalm.base.utils import py23_compat":
            f.write(line)
