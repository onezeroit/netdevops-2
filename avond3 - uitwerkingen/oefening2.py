"""
Oefening 2

Zet LLDP aan op zoveel mogelijk appraten.

"""
from tools_netbox import get_inventory, get_device_details
from tools_napalm import write_config_compare

TENANT = "<tenant>"

config = {
    "vios": "lldp run",
    "iosxr": "lldp",
    "nexus": "feature lldp",
}
inventory = get_inventory(TENANT)
for device_id in inventory:
    device = get_device_details(device_id)
    host = device['primary_ip']
    if device['type'] == "vios":
        changes = config["vios"]
        write_config_compare(host, "ios", changes)
    elif device['type'] == "xrv-9000":
        changes = config["iosxr"]
        write_config_compare(host, "iosxr", changes)
    elif device['type'] == "nexus-9000v":
        changes = config["nexus"]
        write_config_compare(host, "nxos", changes)
