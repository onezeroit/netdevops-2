"""
Oefening 1

Voor elk device uit Netbox:
    Als het een vIOS device is:
    Check of het device “up” is (met device.is_alive())
    Bonus 1: maak gebruik van generieke ‘toolbox’ scripts die je importeert:
        tools_netbox.py
        tools_napalm.py
"""
from tools_netbox import get_inventory, get_device_details
from tools_napalm import check_state

TENANT = "<tenantaam>"
inventory = get_inventory(TENANT)
for device_id in inventory:
    device = get_device_details(device_id)
    if device['type'] == "vios":
        if check_state(device['primary_ip'], "ios")['is_alive']:
            print(f"{device['name']} is up")
        else:
            print(f"{device['name']} is down")
