"""
Voor elk device uit Netbox:
    Maak een backup van de configuratie
"""
from tools_netbox import get_inventory, get_device_details
from tools_napalm import get_config

TENANT = "<tenant>"
inventory = get_inventory(TENANT)
for device_id in inventory:
    device = get_device_details(device_id)
    if device['dev_type'] == "vios":
        driver = "ios"
    elif device['dev_type'] == "xrv-9000":
        driver = "iosxr"
    elif device['dev_type'] == "nexus-9000v":
        driver = "nxos"
    elif device['dev_type'] == "asav":
        driver = "asa"
    else:
        continue  # skip to next device
    config = get_config(device['dev_ip'], driver)
    print(f"writing config for {device['dev_name']}")
    with open(f"configs/{device['dev_name']}.txt", "w") as f: 
        f.write(config)
