"""
Zet configuratie met NAPALM en een Jinja template:	
Een login banner met de volgende informatie:
    Hostname (uit Netbox)
    Site naam (uit Netbox)
    Management IP (uit Netbox)
    Serial number (uit NAPALM facts)
    OS version (uit NAPALM facts
"""
from tools_netbox import get_inventory, get_device_details
from tools_napalm import get_facts, write_config_template

TENANT = "<tenant>"
inventory = get_inventory(TENANT)
for device_id in inventory:
    device = get_device_details(device_id)
    ip_address = device['primary_ip']
    if device['type'] == "vios":
        dev_type = "ios"
    elif device['type'] == "xrv-9000":
        dev_type = "iosxr"
    elif device['type'] == "nexus-9000v":
        dev_type = "nxos"
    else:  # skip naar de volgende device
        continue
    device_facts = get_facts(ip_address, dev_type)
    template_vars = {
        "hostname": device['name'],
        "ip_address": ip_address,
        "site": device['site'],
        "serial": device_facts['serial_number'],
        "os_version": device_facts['os_version'],
    }
    template_name = "banner"
    write_config_template(ip_address, dev_type, template_name, template_vars)
