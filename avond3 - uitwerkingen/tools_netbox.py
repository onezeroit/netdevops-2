"""
Netbox connectie toolbox
    get_device_list: lijst met device id's behorende bij een tenant
    get_device_details: device info op basis van ID
"""
import requests  # import library for making REST API calls

def check_response(status_code):
    """
    herbruikbare status checker... ik schreef deze code meer dan eens.
    """
    if status_code < 300:
        return True
    else:
        raise Exception(f"query failed: code {status_code}")


def get_inventory(tenant):
    """
    haal een lijst op van device id's die bij een tenant horen
    """
    NETBOX_URL = "https://netbox.interestingtraffic.nl"
    query = f"{NETBOX_URL}/api/dcim/devices/?tenant={tenant}"
    response = requests.get(query)
    if check_response(response.status_code):
        content = response.json()

    results = list()
    for device in content['results']:
        results.append(device['id'])
    return results


def get_device_details(device_id):
    """
    haal een lijst op van device id's die bij een tenant horen
    """
    NETBOX_URL = "https://netbox.interestingtraffic.nl"
    query = f"{NETBOX_URL}/api/dcim/devices/{device_id}/"
    response = requests.get(query)
    if check_response(response.status_code):
        content = response.json()
    results = dict()
    results['dev_id'] = device_id
    results['name'] = content['name']
    results['type'] = content['device_type']['slug']
    results['role'] = content['device_role']['slug']
    results['primary_ip'] = content['primary_ip4']['address'].split('/')[0]
    results['site'] = content['site']['name']
    return results


if __name__ == "__main__":
    TENANT = "docent"
    inventory = get_inventory(TENANT)
    sample_details = get_device_details(inventory[0])
    print(sample_details)