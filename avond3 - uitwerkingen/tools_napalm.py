"""
Napalm connectie toolbox
    get_facts: get device facts
    check_state: check of device reageert (met is_alive)
"""
import napalm
import urllib3  # disable insecure warnings for NXOS
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def open_connection(dev_ip, dev_type):
    driver = napalm.get_network_driver(dev_type)
    device = driver(
        hostname=dev_ip,
        username="cisco",
        password="cisco",  # nog steeds hardcoded... foei!
    )
    # open session to device
    device.open()
    return(device)

def get_facts(dev_ip, dev_type):
    '''
    Function to gather 'facts' using NAPALM. Requires:
        dev_ip: device management IP address
        dev_type: NAPALM driver name (ios, iosxr, nxos, asa)
    '''
    device = open_connection(dev_ip, dev_type)
    facts = device.get_facts()
    device.close()
    return facts


def check_state(dev_ip, dev_type):
    '''
    Controleer of device reageert.
    '''
    device = open_connection(dev_ip, dev_type)
    state = device.is_alive()
    device.close()
    return state


def get_config(dev_ip, dev_type):
    '''
    haal running config op
    '''
    device = open_connection(dev_ip, dev_type)
    config = device.get_config(retrieve='running')
    device.close()
    return config['running']


def write_config_compare(dev_ip, dev_type, config):
    '''
    schrijf config weg
        met compare, en ja/nee keus
    '''
    device = open_connection(dev_ip, dev_type)
    # load config
    device.load_merge_candidate(config=config)
    diff = device.compare_config()
    if diff:
        print(f"\n{dev_ip} diff:")
        print(diff)
        # You can commit or discard the candidate changes.
        choice = input("\nWould you like to commit these changes? [yN]: ")
        if choice == "y":
            print("Committing ...")
            device.commit_config()
        else:
            print("Discarding ...")
            device.discard_config()
    else:
        device.discard_config()
    device.close()


def write_config_template(dev_ip, dev_type, tpl_name, template_vars_dict):
    '''
    schrijf config op basis van een Jinja template
    '''
    TPL_PATH = '/home/developer/avond3'
    device = open_connection(dev_ip, dev_type)
    #device.load_merge_candidate(config=config)
    device.load_template(tpl_name, template_path=TPL_PATH, **template_vars_dict)
    print(f"\n{dev_ip} diff:")
    print(device.compare_config())
    device.commit_config()
    device.close()

if __name__ == "__main__":
    test_ip = "10.10.20.181"
    driver = "ios"
    print(check_state(test_ip, driver))
