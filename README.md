# NetDevOps-2

## Doelstelling
In deze training gaan we dieper in op een aantal usecases van Network Automation. In een serie van 4 avonden gaan we een Automation Pipeline bouwen waarmee we geheel automatisch netwerkconfiguraties kunnen uitrollen, testen, valideren en wanneer nodig terugrollen.

Tijdens deze training krijg je hands-on ervaring met (open source) gereedschap zoals NAPALM, NetBox en Vault. Je leert hoe je met deze tools een coherente toolchain kan maken om sneller en betrouwbaarder veranderingen te kunnen doorvoeren in je eigen netwerk.

## Programma
	• Dag 1: Source of Truth: een centrale blik op je netwerk 
	• Dag 2: Configuratiewijzigingen automatiseren
	• Dag 3: Testen en validatie
	• Dag 4: Synthese: alles in één pipeline verwerken

Let op: deze cursus bouwt deels voor op de [Automation 101](https://gitlab.com/onezeroit/netdevops-101) cursus. In elk geval gaan we er vanuit dat je ervaring hebt met Python en Git.
