"""
Oefening 2: query netbox API
 - eigen Tenant
 - filter op 'router' 
 
Bonus: 
 - foutafhandeling op basis van status code
 - maak er een mooie tabel van
"""
import requests  # import library for making REST API calls

# declare fixed values
NETBOX_URL = "https://netbox.interestingtraffic.nl"
TENANT = "docent"
ROLE = "router"

# assemble query
query = f"{NETBOX_URL}/api/dcim/devices/?tenant={TENANT}&role={ROLE}"

# make REST call
response = requests.get(query)
if response.status_code == 200:  # only then it's OK
    content = response.json()
else:
    raise Exception("REST API call failed")

# print nice table
print("{:<15}{:<15}{:<15}{:<15}".format("name","role","make","model"))
for device in content["results"]:
    name = device["display_name"]
    role = device["device_role"]["name"]
    make = device["device_type"]["manufacturer"]["name"]
    model= device["device_type"]["model"]
    print("{:<15}{:<15}{:<15}{:<15}".format(name,role,make,model))
