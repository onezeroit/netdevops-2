"""
Oefening 3b: Voeg aan oefening 3 toe:
 - management interfaces
 - IP addressen
"""
import requests
import yaml

# haal wat functies op uit vorige opdracht
from oefening3 import check_response, device_exist, update_device, role_to_id, type_to_id

# extra herbruikbare functies
def check_ip(address):
    """
    Check of het IP al bestaat. 
     - Zo nee, return 'false'
     - Bij 1 result, return het IP object ID
     - Bij meer dan 1 hit, geef een foutmelding
    """
    query = f"{NETBOX_URL}/api/ipam/ip-addresses/?vrf_id=1&address={address}"
    response = requests.get(query, headers=HEADERS)
    if check_response(response.status_code):
        content = response.json()
    if content['count'] == 0:
        return False
    elif content['count'] == 1:
        return content['results'][0]['id']
    else:
        raise Exception("too many results")


def create_ip(address):
    """
    creeer IP object (met POST method)
    """
    ip_address_exists = check_ip(address)
    if ip_address_exists:
        return ip_address_exists  # returns IP ID
    else:
        data = {"address": address, "tenant": tenant_id, "vrf_id": 1}
        query = f"{NETBOX_URL}/api/ipam/ip-addresses/"
        response = requests.post(query, headers=HEADERS, json=data)
        check_response(response.status_code)
        content = response.json()
        return content['id']


def check_interface(device, interface):
    """
    Check of interface al bestaat. 
     - Zo nee, return 'false'
     - Bij 1 result, return de interface ID
     - Bij meer dan 1 hit, geef een foutmelding
    """
    query = f"{NETBOX_URL}/api/dcim/interfaces/?device_id={device}&name={interface}"
    response = requests.get(query, headers=HEADERS)
    check_response(response.status_code)
    content = response.json()
    if content['count'] == 0:
        return False
    elif content['count'] == 1:
        return content['results'][0]['id']
    else:
        raise Exception("too many results")


def create_interface(device, interface):
    """
    creeer interface object (met POST method)
    """
    interface_id = check_interface(device, interface)
    if interface_id:
        return interface_id
    else:
        data = {
            "device": device,
            "name": interface,
            "type": "virtual"
        }
        query = f"{NETBOX_URL}/api/dcim/interfaces/"
        response = requests.post(query, headers=HEADERS, json=data)
        check_response(response.status_code)
        content = response.json()
        return content['id']


def assign_ip(interface, ip):
    """
    asign IP aan interface (met PATCH method)
    """
    data = {
        "interface": interface,
    }
    query = f"{NETBOX_URL}/api/ipam/ip-addresses/{ip}/"
    response = requests.patch(query, headers=HEADERS, json=data)
    check_response(response.status_code)
    return response.status_code

# daadwerkelijke code uitvoeren
if __name__ == "__main__":
    device_list = open("oefening3b-devices.yml")  # aangevuld met IP addressen
    devices = yaml.load(device_list, Loader=yaml.BaseLoader)

    NETBOX_URL = "https://netbox.interestingtraffic.nl"
    TENANT = "docent"
    TOKEN = "53e1f07d92627202e59f0b0694c190c2f907b975"  # plain text... dat moet slimmer kunnen

    HEADERS = {
        "Authorization":f"Token {TOKEN}",
        "Content-Type": "application/json",
        "Accept": "application/json",
    }

    site_id = 1
    tenant_id = 1  # id van 'docent' tenant

    for device in devices:
        ip_address = f"{device['primary_ip4']}/32"
        ip_address_id = create_ip(ip_address)
        interface_name = "mgmt"

        device_id = device_exist(device['name'])
        if device_id:
            interface_id = create_interface(device_id, interface_name)
            assign_ip(interface_id, ip_address_id)
            device['primary_ip4'] = ip_address_id
            update_device(device_id, device)

        else:
            print(f"Device {device['name']} bestaat nog niet, wordt overgeslagen")
