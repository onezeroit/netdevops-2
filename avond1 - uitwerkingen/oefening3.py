"""
Oefening 3: Voeg (met de REST API) de overige devices toe aan je tenant

    Bonus 1: gebruik een losse YAML file voor je devices
"""
import requests
import yaml  # installeer via 'pip install pyyaml'

# eerst een stel herbruikbare functies maken
def check_response(status_code):
    """
    herbruikbare status checker... ik schreef deze code meer dan eens.
    """
    if status_code < 300:
        return True
    else:
        raise Exception(f"query failed: code {status_code}")

def device_exist(device_name):
    """
    Check of het device al bestaat. 
     - Zo nee, return 'false'
     - Bij 1 result, return het device
     - Bij meer dan 1 hit, geef een foutmelding
    """
    query = f"{NETBOX_URL}/api/dcim/devices/?tenant={TENANT}&name={device_name}"
    response = requests.get(query, headers=HEADERS)
    if check_response(response.status_code):
        content = response.json()
    if content['count'] == 0:
        return False
    elif content['count'] == 1:
        return content['results'][0]['id']
    else:
        raise Exception("too many results")


def create_device(device_dict):
    """
    creeer device (met POST method)
    """
    query = f"{NETBOX_URL}/api/dcim/devices/"
    response = requests.post(query, headers=HEADERS, json=device_dict)
    check_response(response.status_code)
    return response.status_code

def update_device(device_id, device_dict):
    """
    werk device bij (met PUT method)
    """
    query = f"{NETBOX_URL}/api/dcim/devices/{device_id}/"
    response = requests.put(query, headers=HEADERS, json=device_dict)
    check_response(response.status_code)
    return response.status_code

def role_to_id(dev_role):
    """
    functie om het id van een device role op te zoeken
    """
    query = f"{NETBOX_URL}/api/dcim/device-roles/?name={dev_role}"
    response = requests.get(query, headers=HEADERS)
    check_response(response.status_code)
    content = response.json()
    return content['results'][0]['id']


def type_to_id(dev_type):
    """
    functie om het id van een device type op te zoeken
    """
    query = f"{NETBOX_URL}/api/dcim/device-types/?model={dev_type}"
    response = requests.get(query, headers=HEADERS)
    check_response(response.status_code)
    content = response.json()
    return content['results'][0]['id']

# daadwerkelijke code uitvoeren
if __name__ == "__main__":
    device_list = open("oefening3-devices.yml")
    devices = yaml.load(device_list, Loader=yaml.BaseLoader)

    NETBOX_URL = "https://netbox.interestingtraffic.nl"
    TENANT = "docent"
    TOKEN = "53e1f07d92627202e59f0b0694c190c2f907b975"  # plain text... dat moet slimmer kunnen

    HEADERS = {
        "Authorization":f"Token {TOKEN}",
        "Content-Type": "application/json",
        "Accept": "application/json",
    }

    site_id = 1
    tenant_id = 1  # id van 'docent' tenant

    for device in devices:
        device['tenant'] = tenant_id
        device['site'] = site_id
        device['device_type'] = type_to_id(device['device_type'])
        device['device_role'] = role_to_id(device['device_role'])

        device_id = device_exist(device['name'])  # returns 'false' if doesn't exist
        if device_id:
            update_device(device_id, device)
        else:
            create_device(device)
