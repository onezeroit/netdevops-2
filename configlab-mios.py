from __future__ import print_function
import telnetlib
import subprocess
import sys

user = "cisco"
password = "cisco"
#IOS overig
iosdevices = {'172.16.30.54'}
for device in iosdevices:
    tn = telnetlib.Telnet(device)
    tn.read_until(b"Username: ")
    tn.write(user.encode('ascii') + b"\n")
    tn.read_until(b"Password: ")
    tn.write(password.encode('ascii') + b"\n")
    tn.write(b"conf t\n")
    tn.write(b"ip scp server enable\n")
    tn.write(b"archive\n")
    tn.write(b"path flash:archive\n")
    tn.write(b"end\n")
    tn.write(b"exit\n")
    print(tn.read_all().decode('ascii'))
#IOSXR
iosxrdevices = {'172.16.30.55','172.16.30.56','172.16.30.59'}
for device in iosxrdevices:
    tn = telnetlib.Telnet(device)
    tn.read_until(b"Username: ")
    tn.write(user.encode('ascii') + b"\n")
    tn.read_until(b"Password: ")
    tn.write(password.encode('ascii') + b"\n")
    tn.write(b"conf\n")
    tn.write(b"xml agent tty iteration off\n")
    tn.write(b"commit\n")
    tn.read_until(b"(config)#")
    tn.write(b"end\n")
    tn.write(b"exit\n")
    print(tn.read_all().decode('ascii'))
#IOS overig
nxosdevices = {'172.16.30.52','172.16.30.53','172.16.30.57','172.16.30.58'}
for device in nxosdevices:
    tn = telnetlib.Telnet(device)
    tn.read_until(b"login: ")
    tn.write(user.encode('ascii') + b"\n")
    tn.read_until(b"Password: ")
    tn.write(password.encode('ascii') + b"\n")
    tn.write(b"config t\n")
    tn.write(b"feature scp-server\n")
    tn.write(b"feature nxapi\n")
    tn.write(b"end\n")
    tn.write(b"exit\n")
    print(tn.read_all().decode('ascii'))
