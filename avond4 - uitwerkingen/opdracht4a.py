"""
Haal een lijst met interfaces op.
  Voeg in Netbox alle interfaces aan het device toe.
  Vul ook het veldje “mac address” correct in
Bonus (not implemented): haal ook de IP adressen op, en voeg die toe aan de interfaces
"""
from tools_netbox import get_inventory, get_device_details, create_interface
from tools_napalm import get_interfaces
from tools_vault import get_secret

SITE = "<jouw-site-naam>"
VAULT_TOKEN = "<token>"  # vault token, expires after 30 minutes

# get netbox token
token = get_secret("data/<cursist>/netbox", VAULT_TOKEN)['token']
# get cisco pass
credentials = get_secret("data/<cursist>/cisco", VAULT_TOKEN)

# haal inventory op
inventory = get_inventory(SITE, token)
for device_id in inventory:
    device = get_device_details(device_id, token)
    host = device['primary_ip']
    if device['type'] in ["csr-1000v", "vios"]:
        dev_type = 'ios'
    elif device['type'] == "xrv-9000":
        dev_type = "iosxr"
    elif device['type'] == "nexus-9000v":
        dev_type = "nxos"
    else:
        continue
    interfaces = get_interfaces(host, dev_type, credentials)
    print(device['name'])
    for interface_name,details in interfaces.items():
        if interface_name == "Null0":  # IOSXR rapporteert een Null0 interface...
            continue  # die skippen we, is niet interessant.
        elif interface_name.lower() == "loopback0":  # .lower(), omdat het per OS verschilt of het met hoofdletter is
            mac_address = None
            interface_type = "virtual"
        elif "port-channel" in interface_name.lower():
            mac_address = details['mac_address']
            interface_type = "lag"
        elif "vlan" in interface_name.lower():
            mac_address = details['mac_address']
            interface_type = "virtual"
        else:
            mac_address = details['mac_address']
            interface_type = "1000base-t"  # kan slimmer, maar dit werkt
        print(f"setting {interface_name}: {mac_address}")
        create_interface(device_id, interface_name, mac_address, interface_type, token)
