"""
Haal voor elk device de LLDP neighbors op (get_lldp_neighbors()). 
Controleer of in Netbox de juiste interfaces “connected” zijn. Zo niet: corrigeer dat.
	negeer hierbij neighborships over de management interface
"""
from tools_netbox import get_inventory, get_device_details, get_device_interfaces, get_device_id, check_interface, connect_devices
from tools_napalm import get_neighbors
from tools_vault import get_secret

SITE = "<jouw-site-naam>"
VAULT_TOKEN = "<token>"  # vault token, expires after 30 minutes

# get netbox token
token = get_secret("data/<cursist>/netbox", VAULT_TOKEN)['token']
# get cisco pass
credentials = get_secret("data/<cursist>/cisco", VAULT_TOKEN)


# haal inventory op
inventory = get_inventory(SITE, token)


def convert_name(intf_name):
    """
    snelle functie om afgekorte interface namen netjes aan te vullen.
    """
    if "Ethernet" in intf_name:
        canonical_name = intf_name
    elif "Gi" in intf_name:
        canonical_name = intf_name.replace("Gi", "GigabitEthernet")
    elif "Fa" in intf_name:
        canonical_name = intf_name.replace("Fa", "FastEthernet")
    elif "Eth" in intf_name:
        canonical_name = intf_name.replace("Eth", "Ethernet")
    else:
        canonical_name = intf_name
    return canonical_name


for device_id in inventory:
    # haal device details op
    device = get_device_details(device_id, token)
    host = device['primary_ip']
    if device['type'] == "vios":
        dev_type = 'ios'
        management_interface = "GigabitEthernet1"
    elif device['type'] == "xrv-9000":
        dev_type = "iosxr"
        management_interface = "MgmtEth0/RP0/CPU0/0"
    elif device['type'] == "nexus-9000v":
        dev_type = "nxos"
        management_interface = "Management0"
    else:
        continue
    # haal neighbor list op
    neighbors = get_neighbors(host, dev_type, credentials)
    # haal interfaces op uit Netbox
    interfaces = get_device_interfaces(device_id, token)
    print("-------")
    for napalm_name,nb_details in neighbors.items():
        interface_name = convert_name(napalm_name)
        napalm_neighbor = nb_details[0]['hostname'].split('.')[0]  # remove FQDN
        napalm_neighbor_intf = convert_name(nb_details[0]['port'])
        netbox_neighbor = interfaces[interface_name]['neighbor']
        netbox_neighbor_intf = interfaces[interface_name]['neighbor_intf']
        print(f"Netbox: {device['name']} {interface_name} <-> {netbox_neighbor} {netbox_neighbor_intf}")
        print(f"NAPALM: {device['name']} {interface_name} <-> {napalm_neighbor} {napalm_neighbor_intf}")
        # check/corrigeer interface status in Netbox
        if netbox_neighbor != napalm_neighbor or netbox_neighbor_intf != napalm_neighbor_intf:
            # get neighbor interface id from netbox
            neighbor_id = get_device_id(napalm_neighbor, SITE, token)
            neighbor_intf_id = check_interface(neighbor_id, napalm_neighbor_intf, token)
            # create connection (update interface)
            interface_id = interfaces[interface_name]['int_id']
            print(f"updating {interface_name}: connected to interface {neighbor_intf_id}")
            connect_devices(interface_id, neighbor_intf_id, token)
