"""
Napalm connectie toolbox
    get_facts: get device facts
    check_state: check of device reageert (met is_alive)
    get_config: haal running config op
"""
import napalm
from tools_vault import get_secret
import urllib3  # disable insecure warnings for NXOS
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

def open_connection(dev_ip, dev_type, creds):
    """
    herbruikbare code om een NAPALM connectie te openen
    inputs:
     - ip address
     - NAPALM driver (device type)
     - credentials (dictionary met username en password)
    returns een NAPALM device object
    """
    driver = napalm.get_network_driver(dev_type)
    device = driver(
        hostname=dev_ip,
        username=creds['username'],
        password=creds['password']
    )
    # open session to device
    device.open()
    return(device)


def get_facts(dev_ip, dev_type, creds):
    """
    Function to gather 'facts' using NAPALM. Requires:
        dev_ip: device management IP address
        dev_type: NAPALM driver name (ios, iosxr, nxos, asa)
    """
    device = open_connection(dev_ip, dev_type, creds)
    facts = device.get_facts()
    device.close()
    return facts


def check_state(dev_ip, dev_type, creds):
    """
    check state met device.is_alive() method

    returns een boolean (true/false)
    """
    device = open_connection(dev_ip, dev_type, creds)
    state = device.is_alive()
    device.close()
    return state


def get_config(dev_ip, dev_type, creds):
    """
    haal running config op
    returns running config als string
    """
    device = open_connection(dev_ip, dev_type, creds)
    config = device.get_config(retrieve='running')
    device.close()
    return config['running']


def get_interfaces(dev_ip, dev_type, creds):
    """
    haal interface informatie op (show ip interface)
    returns dictionary met interface info:
        https://napalm.readthedocs.io/en/latest/base.html#napalm.base.base.NetworkDriver.get_interfaces
    """
    device = open_connection(dev_ip, dev_type, creds)
    device_interfaces = device.get_interfaces()
    device.close()
    return device_interfaces


def get_neighbors(dev_ip, dev_type, creds):
    """
    haal LLDP neighbors op
    returns neighbors als dictionary:
        https://napalm.readthedocs.io/en/latest/base.html#napalm.base.base.NetworkDriver.get_lldp_neighbors
    """
    device = open_connection(dev_ip, dev_type, creds)
    neighbors = device.get_lldp_neighbors()
    device.close()
    return neighbors


def write_config(dev_ip, dev_type, config, creds):
    """
    write config statements naar device
    print een config diff naar het scherm
    """
    device = open_connection(dev_ip, dev_type, creds)
    # load config
    device.load_merge_candidate(config=config)
    diff = device.compare_config()
    if diff:
        print(f"\n{dev_ip} diff:")
        print(diff)
        device.commit_config()
    else:
        print(f"\n{dev_ip}: no diff")
        device.discard_config()
    device.close()


def write_config_compare(dev_ip, dev_type, config, creds):
    """
    write config statements naar device
    Vraag om bevestiging (y/n)
    """
    device = open_connection(dev_ip, dev_type, creds)
    # load config
    device.load_merge_candidate(config=config)
    print(f"\n{dev_ip} diff:")
    print(device.compare_config())
    # You can commit or discard the candidate changes.
    choice = input("\nWould you like to commit these changes? [yN]: ")
    if choice == "y":
        print("Committing ...")
        device.commit_config()
    else:
        print("Discarding ...")
        device.discard_config()
    device.close()

def write_config_template(dev_ip, dev_type, tpl_name, template_vars_dict, creds):
    """
    write config naar device via Jinja2 template
    Inputs: 
        j2 template
        j2 variabelen als dictionary
    print een config diff naar het scherm
    """
    device = open_connection(dev_ip, dev_type, creds)
    device.load_template(tpl_name, template_path='/home/developer/avond3', **template_vars_dict)
    print(f"\n{dev_ip} diff:")
    print(device.compare_config())
    device.commit_config()
    device.close()

if __name__ == "__main__":
    test_ip = "10.10.20.181"
    driver = "ios"
    TOKEN = "<token>"  #vault token, expires after 30 minutes
    credentials = get_secret("data/<cursist>/cisco", TOKEN)
#    device = get_facts(test_ip, driver)
#    print(device)
    print(check_state(test_ip, driver, credentials))
