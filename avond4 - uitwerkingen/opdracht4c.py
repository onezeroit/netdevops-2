"""
Zet de interface descriptions op basis van de informatie uit Netbox:
    “<neighbor name> | <neighbor role> | <neighbor interface>”
"""
from tools_netbox import get_inventory, get_device_interfaces, get_device_details, get_device_id
from tools_napalm import write_config
from tools_vault import get_secret

SITE = "<jouw-site-naam>"
VAULT_TOKEN = "<token>"  # vault token, expires after 30 minutes

# get netbox token
token = get_secret("data/<cursist>/netbox", VAULT_TOKEN)['token']
# get cisco pass
credentials = get_secret("data/<cursist>/cisco", VAULT_TOKEN)

# haal inventory op
inventory = get_inventory(SITE, token)

for device_id in inventory:
    # haal details op
    device = get_device_details(device_id, token)
    device_ip = device['primary_ip']
    if device['type'] in ["csr-1000v", "vios"]:
        device_type = 'ios'
    elif device['type'] == "xrv-9000":
        device_type = "iosxr"
    elif device['type'] == "nexus-9000v":
        device_type = "nxos"
    else:  # skip naar volgende device
        continue
    # haal interfaces op
    device_interfaces = get_device_interfaces(device_id, token)
    for intf_name,interface in device_interfaces.items():
        if interface['neighbor']:
            intf_id = interface['int_id']
            neighbor_name = interface['neighbor']
            neighbor_intf = interface['neighbor_intf']
            # get device role
            neighbor_id = get_device_id(neighbor_name, SITE, token)
            neighbor_details = get_device_details(neighbor_id, token)
            neighbor_role = neighbor_details['role']
            # maak config statement
            config = (  # dit is een trucje om multiline strings te definieren
                f"interface {intf_name}\n"
                f"  description {neighbor_name} ({neighbor_role}) - {neighbor_intf}\n"
                "end"
            )
            # write config
            print(f"writing config for {device['name']}, interface {intf_name}")
            write_config(device_ip, device_type, config, credentials)
