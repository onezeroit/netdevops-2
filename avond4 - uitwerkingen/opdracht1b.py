from tools_vault import get_user_pass, get_token, write_secret

# get credentials
print("Enter Vault credentials")
vault_creds = get_user_pass()  # returns dict met username en password
print("Enter Cisco credentials")
cisco_creds = get_user_pass()  # returns dict met username en password
netbox_token = input('Netbox token: ')

vault_user = vault_creds['username']
vault_pass = vault_creds['password']
token = get_token(vault_user,vault_pass)  # maak nieuw Vault token

# write Netbox token
print("Writing Netbox Token")
PATH = "data/<cursist>/netbox"
data = {"token": netbox_token}
write_secret(PATH,data,token)

# write Cisco credentials
PATH = "data/<cursist>/cisco"
data = cisco_creds
write_secret(PATH,data,token)
