"""
Netbox connectie toolbox
    get_inventory: lijst met device id's behorende bij een tenant
    get_device_details: device info op basis van ID
    create_interface: voeg network interface toe aan device (met mac address)

"""
import requests  # import library for making REST API calls
from tools_vault import get_secret  # om Netbox token op te halen uit Vault

def check_response(response):
    """
    herbruikbare status checker... ik schreef deze code meer dan eens.
    """
    if response.status_code < 300:
        return True
    else:
        raise Exception(f"query failed: {response.status_code}: {response.reason}")


def query_netbox(url, token):
    """
    herbruikbare code om Netbox URL te bevragen. 
        Input: URL, Netbox token
        Returns: response data (.json())
    """
    headers = {
        "Authorization":f"Token {token}",
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
    response = requests.get(url, headers = headers)
    if check_response(response):
        return response.json()


def post_netbox(url, payload, token, method='post'):
    """
    herbruikbare code om payload naar Netbox URL te schrijven.
        Input: URL, data payload, Netbox token
        Returns: response data (.json())
    """
    headers = {
        "Authorization":f"Token {token}",
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
    if method == 'post':
        response = requests.post(url, headers = headers, json=payload)
    elif method == 'patch':
        response = requests.patch(url, headers = headers, json=payload)
    elif method == 'put':
        response = requests.put(url, headers = headers, json=payload)
    if check_response(response):
        return response.json()


def get_inventory(site, token):
    """
    haal een lijst op van device id's die bij een site horen

    returns: list object met device IDs
    """
    NETBOX_URL = "https://netbox.interestingtraffic.nl"
    query = f"{NETBOX_URL}/api/dcim/devices/?site={site}"
    content = query_netbox(query,token)
    results = list()
    for device in content['results']:
        results.append(device['id'])
    return results


def get_device_id(device_name, site, token):
    """
    haal het device ID op van een device (zodat je device_details kan opvragen)

    input: device name, Site name
    returns: device ID
    """
    NETBOX_URL = "https://netbox.interestingtraffic.nl"
    query = f"{NETBOX_URL}/api/dcim/devices/?site={site}&name={device_name}"
    content = query_netbox(query,token)
    if content['count'] == 1:  # dan is er precies 1 match
        return content['results'][0]['id']
    elif content['count'] > 1:
        raise Exception(f"multiple devices with name {device_name} found in site {site}")
    else:
        return False

def get_device_details(device_id, token):
    """
    haal details van een specific device ID op

    return: een dictionary met de volgende keys:
      dev_id: device id
      name: device name in Netbox
      type: devive type (model)
      role: device role (router, switch, ...)
      primary_ip: management IPv4 adres
      site: site
    """
    NETBOX_URL = "https://netbox.interestingtraffic.nl"
    query = f"{NETBOX_URL}/api/dcim/devices/{device_id}/"
    content = query_netbox(query,token)
    results = dict()
    results['dev_id'] = device_id
    results['name'] = content['name']
    results['type'] = content['device_type']['slug']
    results['role'] = content['device_role']['slug']
    results['primary_ip'] = content['primary_ip4']['address'].split('/')[0]
    results['site'] = content['site']['name']
    return results


def check_interface(device, interface, token):
    """
    check of een interface (naam) al bestaat (precies 1 result)
    zo ja: return interface ID, anders return false
    """
    NETBOX_URL = "https://netbox.interestingtraffic.nl"
    query = f"{NETBOX_URL}/api/dcim/interfaces/?device_id={device}&name={interface}"
    content = query_netbox(query,token)
    if content['count'] == 0:
        return False
    elif content['count'] == 1:
        return content['results'][0]['id']
    else:
        raise Exception("too many results")
    pass


def create_interface(device, interface, mac_address, int_type, token):
    """
    maak interface aan (met mac address)
    """
    NETBOX_URL = "https://netbox.interestingtraffic.nl"
    interface_id = check_interface(device, interface, token)
    if interface_id:
        return interface_id
    else:
        data = {
            "device": device,
            "name": interface,
            "type": int_type,  # OK, dit kan slimmer. Maar dit werkt even snel.
            "mac_address": mac_address
        }
        query = f"{NETBOX_URL}/api/dcim/interfaces/"
        content = post_netbox(query,data,token)
        return content['id']


def get_device_interfaces(device_id, token):
    """
    haal een lijst op van interfaces van een device

    returns dictionary: (vergelijkbaar met NAPALM)
        <interface naam>:
          int_id : interface id
          neighbor: neighbor name
          neighbor_intf: neighbor interface
    """
    NETBOX_URL = "https://netbox.interestingtraffic.nl"
    query = f"{NETBOX_URL}/api/dcim/interfaces/?device_id={device_id}"
    content = query_netbox(query,token)
    results = dict()
    for interface in content['results']:
        name = interface['name']
        int_id = interface['id']
        if interface['connected_endpoint_type'] == "dcim.interface":
            neighbor_name = interface['connected_endpoint']['device']['name']
            neighbor_intf = interface['connected_endpoint']['name']
        else:
            neighbor_name = False
            neighbor_intf = False
        int_details = {
            "int_id": int_id,
            "neighbor": neighbor_name,
            "neighbor_intf": neighbor_intf
        }
        results[name] = int_details
    return results


def connect_devices(interface_a, interface_b, token):
    """
    maak een 'cable' aan om twee devices te verbinden.

    verwacht: 2 interface IDs,
    returns: cable id
    """
    interface_data = {
        "termination_a_type": "dcim.interface",
        "termination_a_id": interface_a,
        "termination_b_type": "dcim.interface",
        "termination_b_id": interface_b,
    }
    NETBOX_URL = "https://netbox.interestingtraffic.nl"
    query = f"{NETBOX_URL}/api/dcim/cables/"
    content = post_netbox(query,interface_data,token,method='post')
    return content['id']


def update_interface(interface_id, data, token):
    """
    update (patch) interface met data
    verwacht een data dictionary data = {key: value, key: value}
    """
    NETBOX_URL = "https://netbox.interestingtraffic.nl"
    query = f"{NETBOX_URL}/api/dcim/interfaces/{interface_id}"
    content = post_netbox(query,data,token,method='patch')
    return content['id']

if __name__ == "__main__":
    SITE= "<jouw-site-name>"
    TOKEN = "<token>"  #vault token, expires after 30 minutes
    netbox_token = get_secret("data/<cursist>/netbox", TOKEN)['token']
    inventory = get_inventory(SITE, netbox_token)
    #sample_details = get_device_details(inventory[0], netbox_token)
    sample_details = get_device_interfaces(inventory[0], netbox_token)
    print(sample_details)