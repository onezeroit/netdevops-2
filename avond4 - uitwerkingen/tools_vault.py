"""
Vault connectie toolbox
    get_token: vraag username/password, haal token op
    get_secret: haal key/value pair op uit Vault
    write_secret: schrijf key/value pair weg naar Vault


Als je dit script runt, vraagt het om Vault username/password en krijg je een token.
"""
import requests
import getpass  # input password zonder te tonen op scherm

def check_response(status_code):
    """
    herbruikbare status checker (gekopieerd uit tools_netbox)
    """
    if status_code < 300:
        return True
    else:
        raise Exception(f"query failed: code {status_code}")

def get_user_pass():
    """
    vraag om username en password
    return dict met username en password
    """
    creds = dict()  # initialize emtpy results dictonary
    creds['username'] = input('Username: ')
    creds['password'] = getpass.getpass(prompt='Password: ')
    return creds

def get_token(user,password):
    """
    Creeer Vault token met een gegeven username/password
    return het token
    """

    VAULT = "https://vault.interestingtraffic.nl:8200"
    query = f"{VAULT}/v1/auth/userpass/login/{user}"
    # post een JSON dictionary met het password naar de userpass url van de user
    response = requests.post(query, json={"password": password})
    if check_response(response.status_code):
        content = response.json()
        token = content['auth']['client_token']
        return token


def get_secret(path,token):
    """
    haal secret op:
        curl --request GET https://vault/v1/secret/data/<>

    return de key-value pairs die in Vault staan
    """
    VAULT = "https://vault.interestingtraffic.nl:8200"
    query = f"{VAULT}/v1/secret/{path}"
    headers = { "X-Vault-Token": token }
    response = requests.get(query, headers=headers)
    if check_response(response.status_code):
        content = response.json()
        return content['data']['data']


def write_secret(path,secret,token):
    """
    schrijf 'secret' weg als Vault data
        curl --request POST --data @payload.json https://vault/v1/secret/data/<>

    'secret' die je mee geeft is een dictionary, wordt in Vault opgeslagen als key/value pairs

    returns secret version
    """
    VAULT = "https://vault.interestingtraffic.nl:8200"
    query = f"{VAULT}/v1/secret/{path}"
    headers = { "X-Vault-Token": token }
    data = {"data": secret }
    response = requests.post(query, headers=headers, json=data)
    if check_response(response.status_code):
        content = response.json()
        return content['data']['version']

if __name__ == "__main__":
    credentials = get_user_pass()
    vault_user = credentials['username']
    vault_pass = credentials['password']
    token = get_token(vault_user,vault_pass)
    print(f"Vault token voor {vault_user} is {token}")
