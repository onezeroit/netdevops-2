"""
Oefening 2

Zet LLDP aan op zoveel mogelijk appraten.

"""
from tools_netbox import get_inventory, get_device_details
from tools_napalm import write_config
from tools_vault import get_secret

SITE = "<jouw-site-naam>"
VAULT_TOKEN = "<token>"  # vault token, expires after 30 minutes

# verschillende benodigde config statements per device type
config = {
    "ios": "lldp run",
    "iosxr": "lldp",
    "nexus": "feature lldp",
}

# get netbox token
token = get_secret("data/<cursist>/netbox", VAULT_TOKEN)['token']
# get cisco pass
credentials = get_secret("data/<cursist>/cisco", VAULT_TOKEN)

inventory = get_inventory(SITE, token)  # alle device IDs in de site
for device_id in inventory:
    device = get_device_details(device_id, token)  # haal device details op
    host = device['primary_ip']
    if device['type'] in ["csr-1000v", "vios"]:
        changes = config["ios"]
        write_config(host, "ios", changes, credentials)
    elif device['type'] == "xrv-9000":
        changes = config["iosxr"]
        write_config(host, "iosxr", changes, credentials)
    elif device['type'] == "nexus-9000v":
        changes = config["nexus"]
        write_config(host, "nxos", changes, credentials)
    else:
        continue
