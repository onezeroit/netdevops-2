"""
Oefening 1
    Haal een lijst met Devices/IP-adressen op uit Netbox

Oefening 2
    Loop over de lijst van IP-adressen, kijk of je ze kan pingen.
"""
import requests  # import library for making REST API calls
import os  # voor 'ping' commando


def get_inventory(tenant):
    NETBOX_URL = "https://netbox.interestingtraffic.nl"
    query = f"{NETBOX_URL}/api/dcim/devices/?tenant={tenant}"

    response = requests.get(query)
    if response.status_code == 200:  # only then it's OK
        content = response.json()
    else:
        raise Exception("REST API call failed")

    return content['results']


def parse_devices(device_list):
    results = dict()
    for device in device_list:
        dev_id = device['id']
        results[dev_id] = dict()
        results[dev_id]['dev_name'] = device['name']
        results[dev_id]['dev_type'] = device['device_type']['slug']
        results[dev_id]['dev_role'] = device['device_role']['slug']
        results[dev_id]['dev_ip'] = device['primary_ip4']['address'].split('/')[0] #alleen alles voor de '/'
    return results


def check_ping(hostname):
    '''
    ping het IP adres. Dit gebruikt 'Linux' syntax, Windows heeft een ander commando nodig.
        " >/dev/null 2>&1" zorgt ervoor dat het verder geen output op het scherm zet.
    '''
    response = os.system("ping -c 1 " + hostname + " >/dev/null 2>&1")
    if response == 0:
        return True
    else:
        return False

if __name__ == "__main__":
    TENANT = "docent"
    inventory = get_inventory(TENANT)
    devices = parse_devices(inventory)
    for id,device in devices.items():
        ping_test = check_ping(device['dev_ip'])
        print(f"{device['dev_name']} online: {ping_test}")
