"""
Oefening 3

Voor elk device uit Netbox:
    Als het geen ‘host’ is:
    Haal het serienummer op

Bonus: schrijf het serienummer weg in het juiste dataveld in Netbox
"""
import napalm
import requests
from oefening1 import get_inventory, parse_devices

def get_facts(dev_ip, dev_type):
    '''
    Function to gather 'facts' using NAPALM. Requires:
        dev_ip: device management IP address
        dev_type: NAPALM driver name (ios, iosxr, nxos, asa)
    '''
    driver = napalm.get_network_driver(dev_type)
    device = driver(
        hostname=dev_ip,
        username="cisco",
        password="cisco",  # nog steeds hardcoded... foei!
    )

    # open session to device
    device.open()
    # execute commands
    facts = device.get_facts()
    # close the session
    device.close()
    return facts


def update_device(device_id, device_dict):
    NETBOX_URL = "https://netbox.interestingtraffic.nl"
    TENANT = "<tenantnaam>"  # misschien handiger om als variabele aan de functie mee te geven
    TOKEN = "<token>"  # plain text... dat moet slimmer kunnen
    headers = {
        "Authorization":f"Token {TOKEN}",
        "Content-Type": "application/json",
        "Accept": "application/json",
    }
    query = f"{NETBOX_URL}/api/dcim/devices/{device_id}/"
    response = requests.patch(query, headers=headers, json=device_dict)
    if response.status_code <= 300:
        return response.status_code
    else:
        raise Exception("update failed")


if __name__ == "__main__":
    TENANT = "<tenantnaam>"
    inventory = get_inventory(TENANT)
    devices = parse_devices(inventory)
    for dev_id,device in devices.items():  # loop over de resultaten van de query
        if device['dev_type'] == "vios":
            facts = get_facts(device['dev_ip'],"ios")
        elif device['dev_type'] == "xrv-9000":
            facts = get_facts(device['dev_ip'],"iosxr")
        elif device['dev_type'] == "nexus-9000v":
            continue  # NXOS devices zijn offline, dus sla deze over
            facts = get_facts(device['dev_ip'],"nxos")
        elif device['dev_type'] == "asav":
            facts = get_facts(device['dev_ip'],"asa")
        else:
            continue  # ga door naar het volgende device
        serial = facts['serial_number']
        print(f"{device['dev_name']}: {serial}")
        dev_update = {'serial': serial}
        update_device(dev_id,dev_update)
