"""
Voor elk device uit Netbox:
    Maak een backup van de configuratie
"""
import napalm
from oefening1 import get_inventory, parse_devices

def get_config(dev_ip, dev_type):
    driver = napalm.get_network_driver(dev_type)
    device = driver(
        hostname=dev_ip,
        username="cisco",
        password="cisco",
    )

    # open session to device
    device.open()
    # execute commands
    config = device.get_config(retrieve='running')
    # close the session
    device.close()
    return config['running']

if __name__ == "__main__":
    TENANT = "<tenantnaam>"
    inventory = get_inventory(TENANT)
    devices = parse_devices(inventory)
    for dev_id,device in devices.items():
        if device['dev_type'] == "vios":  # dit kan veel slimmer, maar zo werkt het ook.
            config = get_config(device['dev_ip'],"ios")
        elif device['dev_type'] == "xrv-9000":
            config = get_config(device['dev_ip'],"iosxr")
        elif device['dev_type'] == "asav":
            config = get_config(device['dev_ip'],"asa")
        else:
            continue
        print(f"writing config for {device['dev_name']}")
        with open(f"configs/{device['dev_name']}.txt", "w") as f: 
            f.write(config)
